#pragma once

#ifndef BODY_H
#define BODY_H

#include <NDK/Application.hpp>
#include <NDK/Components.hpp>
#include <NDK/Systems.hpp>
#include <NDK/World.hpp>
#include <NDK/LuaAPI.hpp>
#include <Nazara/Core.hpp>
#include <Nazara/Lua.hpp>
#include <Nazara/Graphics.hpp>
#include <Nazara/Renderer.hpp>
#include <Nazara/Utility.hpp>
#include <iostream>
#include <functional>
#include <vector>

#include "../include/Skill.h"
#include "../include/Tools.h"

class Body;

using BodyUpdateFunction = std::function<void(Body*, const float)>;


class Body
{
	public:
		class Anim
		{
			public:
				Anim(Nz::String n, short fRate, short fCount);

				void Animate(const float delta, Nz::SpriteRef sp);

				Nz::Rectui GetRect();
			private:
				float accum = 0;
				short frame = 0;
				short frameRate;
				short frameCount;
				Nz::Rectui rect;
				Nz::String name;
		};


		Body(BodyUpdateFunction update, Ndk::EntityHandle entity, SkillList& skillList, const std::vector<Anim>& anims);
		~Body();
		void Update(const float delta);

		Ndk::EntityHandle GetEntity();
	private:
		Nz::SpriteRef m_sprite;
		Nz::TextureRef t;

		Nz::BoxCollider2DRef m_collider;

		Ndk::EntityHandle m_entity;

		Ndk::GraphicsComponent& m_graphics;
		Ndk::NodeComponent& m_node;
		Ndk::PhysicsComponent2D& m_physics;
		Ndk::CollisionComponent2D& m_collision;

		SkillList& skills;

		BodyUpdateFunction m_update;

		std::vector<Anim> m_anims;
		unsigned short m_currentAnim;
};

void Player_Update(Body*, const float);

#endif
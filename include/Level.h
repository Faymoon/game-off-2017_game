#pragma once

#ifndef LEVEL_H
#define LEVEL_H

#include <Nazara/Utility.hpp>
#include <Nazara/Graphics/TileMap.hpp>
#include <Nazara/Physics2D/Collider2D.hpp>
#include <Nazara/Core/File.hpp>
#include <NDK/World.hpp>
#include <NDK/Entity.hpp>
#include <NDK/State.hpp>
#include <NDK/Components/CameraComponent.hpp>
#include <NDK/Components/NodeComponent.hpp>
#include <NDK/Components/GraphicsComponent.hpp>

#include "../include/Body.h"

class Level : public Ndk::State
{
	public:
		Level(const Nz::String tileSetPath, const Nz::String tileMapPath, const Nz::String colliderMapPath, Ndk::WorldHandle w);
		~Level() = default;
	private:
		void Enter(Ndk::StateMachine& fsm) override;
		void Leave(Ndk::StateMachine& fsm) override;
		bool Update(Ndk::StateMachine& fsm, float elapsedTime) override;

		static Nz::TileMapRef LoadTileMap(const Nz::String& tileSetPath, const Nz::String& tileMapPath, Nz::MaterialRef tileMat);
		static Nz::CompoundCollider2DRef LoadLevelCollider(const Nz::String& colliderMapPath);

		Nz::CompoundCollider2DRef m_collider;

		Nz::TileMapRef m_tileMap;
		Nz::MaterialRef  m_tileMat;

		//Body b;

		Ndk::WorldHandle world;
};

#endif
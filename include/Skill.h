#pragma once

#ifndef SKILL_H
#define SKILL_H

#include <NDK/Application.hpp>
#include <NDK/Components.hpp>
#include <NDK/Systems.hpp>
#include <NDK/World.hpp>
#include <NDK/LuaAPI.hpp>
#include <Nazara/Core.hpp>
#include <Nazara/Lua.hpp>
#include <Nazara/Graphics.hpp>
#include <Nazara/Renderer.hpp>
#include <Nazara/Utility.hpp>
#include <functional>
#include <string>

class Skill;
class SkillList;

class Skill
{
	public:
		enum class Type
		{
			FIREBALL,
			SHIELD,
			HEALTH,
			DASH,
			ICEBALL,
			FLY
		};

		using SkillUpdateFunction = std::function<void(Ndk::EntityHandle, Ndk::EntityHandle, Skill::Type, short int, const float)>;

		Skill(SkillUpdateFunction func, Ndk::EntityHandle entity, Ndk::EntityHandle parent, Skill::Type type, short int level);
		~Skill();

		void Update(const float delta);
	private:
		Type m_type;
		short int m_level;

		Nz::SpriteRef m_sprite;

		Ndk::EntityHandle m_parent;
		Ndk::EntityHandle m_entity;

		Ndk::GraphicsComponent& m_graphics;
		Ndk::NodeComponent& m_node;

		SkillUpdateFunction m_update;
};

class SkillList : public std::vector<Skill>
{
	public:
		template <typename... Args>
		void AddSkill(Args&&... args);


		void Update(const float delta);
};

#endif

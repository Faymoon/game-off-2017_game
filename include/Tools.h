#pragma once

#ifndef TOOLS_H
#define TOOLS_H

#include <Nazara/Core/String.hpp>

class Tools
{
    public:
        static Nz::String IntWithZeros(unsigned short number, unsigned short size);
};

#endif
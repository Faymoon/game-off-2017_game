#include "../include/Body.h"


Body::Body(BodyUpdateFunction update, Ndk::EntityHandle entity, SkillList& skillList, const std::vector<Anim>& anims)
	:
	m_entity{entity},

	m_graphics{ m_entity->AddComponent<Ndk::GraphicsComponent>() },
	m_node{ m_entity->AddComponent<Ndk::NodeComponent>() },
	m_physics{ m_entity->AddComponent<Ndk::PhysicsComponent2D>() },
	m_collision{ m_entity->AddComponent<Ndk::CollisionComponent2D>() },

	m_sprite{ Nz::Sprite::New() },
	t{ Nz::Texture::New() },

	m_collider{ Nz::BoxCollider2D::New(Nz::Vector2f(100, 100)) },

	skills{ skillList },

	m_update{ update },

	m_anims{ anims },
	m_currentAnim{ 0 }
{
	m_physics.SetMass(1);

	m_collision.SetGeom(m_collider);

	m_node.SetScale(5);

	m_sprite->SetTexture(m_anims[m_currentAnim].GetTextures()[3]);
	m_sprite->SetOrigin(m_sprite->GetSize() / 2);

	m_graphics.Attach(m_sprite);
}


Body::~Body()
{
}

void Body::Update(const float delta)
{
	m_update(this, delta);
	
	m_anims[m_currentAnim].Animate(delta, m_sprite);
}

Ndk::EntityHandle Body::GetEntity()
{
	return m_entity;
}


void Player_Update(Body* player, const float delta)
{
	//if (Nz::Keyboard::IsKeyPressed(Nz::Keyboard::Space))
	//	player->GetComponent<Ndk::PhysicsComponent2D>().SetPosition(Nz::Vector2f(0, 0));
}

Body::Anim::Anim(Nz::String n, short fRate, short fCount)
	:
	textures(fCount, Nz::Texture::New()),

	name{ n },
	frameCount{ fCount },
	frameRate{ fRate }
{
	unsigned short i = 0;
	for (unsigned short i = 0; i < frameCount; i++)
	{
		std::cout << "resources/Player/" + name + "-" + Tools::IntWithZeros(i, 3) + ".png" << std::endl;

		textures[i]->LoadFromFile("resources/Player/" + name + "-" + Tools::IntWithZeros(i, 3) + ".png");
	}
}


void Body::Anim::Animate(const float delta, Nz::SpriteRef sp)
{
	accum += delta;
	if (accum >= 1.f / frameRate)
	{
		frame++;
		accum = accum - (1.f / frameRate);
	}
	if (frame >= frameCount )
		frame = 0;

	sp->SetTexture(textures[frame]);
}

std::vector<Nz::TextureRef>& Body::Anim::GetTextures()
{
	return textures;
}

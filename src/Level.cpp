#include "../include/Level.h"

Level::Level(const Nz::String tileSetPath, const Nz::String tileMapPath, const Nz::String colliderMapPath, Ndk::WorldHandle w)
	:
	m_tileMap{ LoadTileMap(tileSetPath, tileMapPath, m_tileMat) },
	m_collider{ LoadLevelCollider(colliderMapPath) },

	world{ w }/*,

	b{ &Player_Update, world->CreateEntity(), "resources/1.png", SkillList() }*/
{
}


void Level::Enter(Ndk::StateMachine&)
{
	Ndk::EntityHandle tilemap = world->CreateEntity();
	tilemap->AddComponent<Ndk::NodeComponent>();
	tilemap->AddComponent<Ndk::GraphicsComponent>().Attach(m_tileMap);
}

bool Level::Update(Ndk::StateMachine&, float delta)
{
	//b.Update(delta);
	return true;
}

void Level::Leave(Ndk::StateMachine&)
{
}

Nz::CompoundCollider2DRef Level::LoadLevelCollider(const Nz::String& colliderMapPath)
{
	return  Nz::CompoundCollider2D::New(std::vector<Nz::Collider2DRef>());
}

Nz::TileMapRef Level::LoadTileMap(const Nz::String& tileSetPath, const Nz::String& tileMapPath, Nz::MaterialRef tileMat)
{
	/**/Nz::File file{ tileMapPath };

	file.Open(Nz::OpenMode_ReadOnly);

	Nz::String line{ file.ReadLine() };

	Nz::TileMapRef tileMap{ Nz::TileMap::New(Nz::Vector2ui(96, 48), Nz::Vector2f(48.f, 48.f)) };

	tileMat =  Nz::Material::New() ;
	tileMat->LoadFromFile("resources/tileSet.png");

	tileMap->SetMaterial(0, tileMat);

	tileMap->EnableTile(Nz::Vector2ui(0, 0), Nz::Rectui(48, 0));

	/*for(int j=0; !line.IsEmpty(); j++)
	{
		for (unsigned int i = 0; i < line.GetSize() - 1; i++)
		{
			unsigned int u = Nz::StringToNumber(Nz::String(line[i])) + 1;
			std::cout << i << " et " << j << std::endl;
			tileMap->EnableTile(Nz::Vector2ui(i * 48, j * 48), Nz::Rectui(u * 48, 0));
		} 
		line = file.ReadLine();
	}*/

	return tileMap;
}

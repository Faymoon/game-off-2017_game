#include "../include/Skill.h"



Skill::Skill(SkillUpdateFunction func, Ndk::EntityHandle entity, Ndk::EntityHandle parent, Skill::Type type, short int level)
	:
	m_sprite{ Nz::Sprite::New() },

	m_type{ Skill::Type(type) },
	m_level{ level },

	m_entity{ entity },
	m_parent{ parent },

	m_graphics{ m_entity->AddComponent<Ndk::GraphicsComponent>() },
	m_node{ m_entity->AddComponent<Ndk::NodeComponent>() },

	m_update{ func }
{
	m_sprite->SetTexture("resources/" + std::to_string(int(m_type)) + ".png");

	m_graphics.Attach(m_sprite);
}

Skill::~Skill()
{
}


void Skill::Update(const float delta)
{
	m_update(m_entity, m_parent, m_type, m_level, delta);
}

template <typename... Args>
void SkillList::AddSkill(Args&&... args)
{
	push_back(Skill(std::forward<Skill>(args). . .));
}

void SkillList::Update(const float delta)
{
	for (auto i = begin(); i < end(); i++)
	{
		i->Update(delta);
	}
}
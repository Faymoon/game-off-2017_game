#include "../include/Tools.h"


Nz::String Tools::IntWithZeros(unsigned short number, unsigned short size) {
	Nz::String chaine = Nz::String::Number(number);
	while(chaine.GetSize() < size)
	{
	    chaine="0"+chaine;
	}
	return chaine;
}
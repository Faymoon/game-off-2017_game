#include <Nazara/Core.hpp>
#include <Nazara/Lua.hpp>
#include <Nazara/Graphics.hpp>
#include <Nazara/Renderer.hpp>
#include <Nazara/Utility.hpp>
#include <NDK/Application.hpp>
#include <NDK/Components.hpp>
#include <NDK/Systems.hpp>
#include <NDK/World.hpp>
#include <NDK/LuaAPI.hpp>
#include <NDK/Lua/LuaBinding_Base.hpp>
#include <NDK/StateMachine.hpp>
#include <iostream>
#include "../include/Body.h"
#include "../include/Skill.h"
#include "../include/Level.h"

int main()
{
	Ndk::Application app;

	Nz::RenderWindow& win = app.AddWindow<Nz::RenderWindow>();
	win.Create(Nz::VideoMode(800, 600, 32), "Test");

	Ndk::World& world = app.AddWorld();
	world.GetSystem<Ndk::RenderSystem>().SetGlobalUp(Nz::Vector3f::Down());

	Nz::PhysWorld2D& physWorld = world.AddSystem<Ndk::PhysicsSystem2D>().GetWorld();
	physWorld.SetGravity(Nz::Vector2f(0, 9.81));

	Ndk::EntityHandle camEntity = world.CreateEntity();
	Ndk::NodeComponent& cam = camEntity->AddComponent<Ndk::NodeComponent>();
	
	Ndk::CameraComponent& camComp = camEntity->AddComponent<Ndk::CameraComponent>();
	camComp.SetTarget(&win);
	camComp.SetProjectionType(Nz::ProjectionType_Orthogonal);
	/*
	auto solEntity = world.CreateEntity();
	solEntity->AddComponent<Ndk::NodeComponent>().SetPosition(Nz::Vector3f(0, 500, 0));

	/*Nz::BoxCollider2DRef collider{ Nz::BoxCollider2D::New(Nz::Vector2f(100, 500)) };
	solEntity->AddComponent<Ndk::CollisionComponent2D>().SetGeom(collider);
	solEntity->AddComponent<Ndk::PhysicsComponent2D>()
	SkillList skills{};

	Body player = Body(world.CreateEntity(), Nz::String("resources/1.png"), );

	//skills.AddSkill("skill_update", world.CreateEntity(), player.GetEntity(), Skill::Type::SHIELD, 1);
	*/


	//Ndk::StateMachine fsm{ std::make_shared<Level>("resources/tileSet.png", "resources/tileMap", "resources/colliderMap", world.CreateHandle()) };

	Body::Anim anim{"running", 12, 9};

	Body b( &Player_Update, world.CreateEntity(), SkillList(), std::vector<Body::Anim>(1, anim) );

	while (app.Run())
	{
		b.Update(app.GetUpdateTime());
		//fsm.Update(app.GetUpdateTime());

		win.Display();
	}

	return EXIT_SUCCESS;
}